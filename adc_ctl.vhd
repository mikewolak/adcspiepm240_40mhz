LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY adc_ctl IS

	PORT (
		CLK : IN STD_LOGIC; -- System Clock
		RST : IN STD_LOGIC; -- Reset
		STAT : IN STD_LOGIC; -- Poll ADC for conversion complete
		TRG : IN STD_LOGIC; -- Sample Clock Trigger
		ACQ : IN STD_LOGIC; -- Atmel control signal to start / stop sampling. Active High. 
		RDY : OUT STD_LOGIC := '1'; -- ADC data valid, this controls the data latch to Atmel, active low
		CONV : OUT STD_LOGIC := '1' -- Instruct ADC to start conversion, active low
	);
END adc_ctl;

ARCHITECTURE RTL OF adc_ctl IS

	TYPE adc_ctl_STATE IS (start, convert, chold0, chold1, poll, trgwait, done);
	SIGNAL STATE : adc_ctl_STATE := start;

BEGIN
	PROCESS (CLK, RST, TRG, STAT, ACQ)
	BEGIN
		IF ((RST = '0') OR (ACQ = '0')) THEN
			CONV <= '1';
			RDY <= '1';
			STATE <= start;
		ELSE
			IF (rising_edge(CLK) AND ACQ = '1') THEN -- start conversion when system clock and ACQ pin high
				CASE STATE IS
					WHEN start =>
						IF (STAT = '0') AND (TRG = '0') THEN -- ADC idle, sample clock trigger asserted
							STATE <= convert; -- start new conversion
						ELSE
							STATE <= start;   -- conversion in progress or sample clock high
						END IF;
					WHEN convert => -- hold conversion pin low for 2 clock periods 
						CONV <= '0';	-- CONV 0 starts ADC conversion
						STATE <= chold0;
					WHEN chold0 =>		-- assert CONV low for addition clock cycle
						STATE <= chold1;
					WHEN chold1 =>
						CONV <= '1';   --  dissert CONV pin
						STATE <= poll;
					WHEN poll => -- poll stat pin until it returns low
						IF (STAT = '1') THEN
							STATE <= poll; -- poll for conversion complete status
						ELSE
							STATE <= trgwait;
						END IF;
					WHEN trgwait =>		--- wait until sample clock trigger returns high
						IF TRG = '0' THEN
							STATE <= trgwait;
						ELSE
							STATE <= done;
						END IF;
					WHEN done =>
						STATE <= start;
					WHEN OTHERS =>
						STATE <= start;
				END CASE;
			END IF;
		END IF; -- RST

		RDY <= STAT;-- When STAT low, ADC data valid.

	END PROCESS;
END RTL;