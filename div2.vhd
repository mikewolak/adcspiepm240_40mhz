LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY div2 IS
	PORT (
		CLK_O : OUT std_logic; -- Output clock
		enable : IN std_logic; -- Enable counting
		CLK_I : IN std_logic; -- Input clock
		reset : IN std_logic -- Input reset
	);
END ENTITY;

ARCHITECTURE rtl OF div2 IS
	SIGNAL clk_div : std_logic;
BEGIN
	PROCESS (CLK_I, reset) BEGIN
		IF (reset = '0') THEN
			clk_div <= '0';
		ELSIF (rising_edge(CLK_I)) THEN
			IF (enable = '1') THEN
				clk_div <= NOT clk_div;
			END IF;
		END IF;
	END PROCESS;
	CLK_O <= clk_div;
END ARCHITECTURE;