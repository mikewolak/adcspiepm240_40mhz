LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
ENTITY dbusmux IS
	PORT (

		i_clk : IN STD_LOGIC; -- sys clock
		i_rst : IN STD_LOGIC; -- sys reset
		i_msblsb : IN STD_LOGIC; -- MSB or LSB output on o_datalatch
		i_adcdatabus : IN STD_LOGIC_VECTOR (11 DOWNTO 0); -- full 12-bit ADC databus
		i_datastable : IN STD_LOGIC; -- Data availble when ADC STATUS = 0 (not in a conversion)
		o_dataready : OUT STD_LOGIC := '1'; -- high to low tranistions interrupts Amtel to read msb/lsb
		o_datalatch : OUT STD_LOGIC_VECTOR (7 DOWNTO 0) := (OTHERS => '0')); -- 8-bit data output 
END dbusmux;

ARCHITECTURE rtl OF dbusmux IS
	SIGNAL busdata : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
	SIGNAL dataready : STD_LOGIC := '1';
BEGIN
	muxlatch : PROCESS (i_clk, i_rst, i_msblsb, i_adcdatabus)
	BEGIN
		IF (i_rst = '0') THEN
			busdata <= (OTHERS => '0');
			dataready <= '1';
		ELSIF (rising_edge(i_clk)) THEN -- sync to sys clock
			IF (i_datastable = '0') THEN -- if 0 latch data
				dataready <= '0'; -- this will trigger atmel interrupt to latch in msb/lsb data 
				IF (i_msblsb = '1') THEN
					busdata <= "0000" & i_adcdatabus(11 DOWNTO 8);
				ELSE
					busdata <= i_adcdatabus(7 DOWNTO 0);
				END IF;
			ELSE
				dataready <= '1';
			END IF;

		END IF; -- rising clock edge
	END PROCESS muxlatch;

	o_datalatch <= busdata;
	o_dataready <= dataready;
END rtl;