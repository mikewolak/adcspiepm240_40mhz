LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ledcontroller IS

	PORT (
		CLK : IN STD_LOGIC; -- System Clock
		RST : IN STD_LOGIC;
		TST : IN STD_LOGIC;
		ADCDATA : IN STD_LOGIC_VECTOR (11 DOWNTO 0); -- 12-Bit ADC DATA
		LEDDATA : OUT STD_LOGIC_VECTOR (15 DOWNTO 0) -- LED DATA
	);
END ledcontroller;
ARCHITECTURE RTL OF ledcontroller IS

	TYPE LED_STATE IS (idle, done);
	SIGNAL state : LED_STATE := idle;
BEGIN

	--LEDDATA <= ADCDATA;
	--LEDDATA(LEDDATA'high downto 0) <= (others => '0');
	PROCESS (CLK, RST, TST, ADCDATA )	
	BEGIN
		IF (TST = '0') THEN
			LEDDATA(15 DOWNTO 0) <= x"FFFF";
		ELSE
			LEDDATA(15 DOWNTO 4) <= ADCDATA(11 DOWNTO 0);
			LEDDATA(3 DOWNTO 0) <= "0000";
		END IF;
	
		IF (RST = '0') THEN
			LEDDATA <= x"0000";
			state <= idle;
		ELSIF rising_edge(CLK) THEN
			CASE state IS
				WHEN idle =>
					state <= done;
				WHEN done =>
					state <= idle;
				WHEN OTHERS =>
					state <= idle;
			END CASE;

		END IF; -- RST
	END PROCESS;
END RTL;